#!/usr/bin/perl

# parses two columns of text input (date, post) looking for URLs
# outputs date <tab> url
# copes with lack of clear initial word boundary (e.g. "...http://foo.bar") 
# date: 2014-07-01

while (<STDIN>) 
{
	chomp;
	@words = split(/\s/, $_);
	$date = shift(@words);
	foreach $word (@words) 
	{
		if ($word =~ m/(http:\/\/[a-z|0-9|\/|\.|\?|=|-|_|%]*)/i)
		{
			print  "$date\t$1\n";
		}
	}
}