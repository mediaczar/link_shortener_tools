# Link Shortener Queries #

This is a series of scripts I use to query the APIs of various link shorteners to collect click data.

## bitlyquery ##

### Short description ###
Takes a list of **`bit.ly`** shortened URLs and outputs tab-delimited click and expanded URL data.
### Longer description
You may already know that you can add a '+' to any bit.ly link (or link that uses the bit.ly shortener) to view public data on that link. Here's an example: [http://bitly.com/RNfp5T+](http://bitly.com/RNfp5T+)

`bitlyquery` is a helper script that queries the [bit.ly API](http://dev.bitly.com/link_metrics.html) and collects click counts and expanded URL data. This lets me process masses of data from 3rd party social media accounts - without having to go through each individual link.

The script comes in two flavours: a perl script I wrote in 2012 and a newer python script (I'm slowly updating the scripts I still use into python.) Only the python script will be maintained.
 
### Authentication ###

You'll need to get an access token by following [these instructions](http://dev.bitly.com/authentication.html#basicauth).  

### How I use it ###
I can use the python script in two ways; either feed it a file with a list of short links:

````
bitlyquery.py list.txt
````

Or I can use it as part of a series of piped commands.  Let's say I've collected a bunch of Tweets or Facebook Posts into a file, `example.csv`. I fire up a terminal and type:

````
grep -io 'http://[a-z0-9./?&$!+#_=-]*' example.csv | grep 'bit.ly' | sort -u | bitlyquery.pl > clicks.txt
````

What's happening here?

1. "`grep -io 'http://[a-z0-9./?&$!+#_=-]*' <filename>` ": I'm searching (grepping) through the `example.csv` file for things that look like URLs (I'm using the regular expression `'http://[a-z0-9./?&$!+#_=-]*'`), and then only returning those things. the `-io` flags tell `grep` to ***i***gnore case and ***o***nly return matches. I'm collecting *all* the URLs (which is a bit of a long way around; but it's a useful regular expression, so I use it often and have it to hand.)
2. "`| grep 'bit.ly'`": The pipe ("`|`") character tells the computer to take the output of the last command as the input for this command. Now I'm just filtering the output of the last command for links that look like bit.ly links.
3. "`| sort -u`": I'm piping all those bit.ly links into a `sort` command - which I'd normally use to organise things in alphabetical or numeric order. In this case, I'm using the `-u` flag to remove duplicates, leaving only ***u***nique links. So now I've got a list of unique bit.ly links that I can pipe into...
4.  "`| bitlyquery.pl`": the script. This does all the work of talking to the [bit.ly API](http://dev.bitly.com/link_metrics.html) and asking for the data.
5.  "`> clicks.txt`": take the output of `bitlyquery.pl` and save it into a file called `clicks.txt`.  I can then open the file in Excel or `R` for analysis and charting.

### Known Bugs & Issues ###

The perl scripts uses the [deprecated](http://dev.bitly.com/deprecated.html#v3_clicks) `/clicks` method. The python version has been [updated](http://dev.bitly.com/link_metrics.html#v3_link_clicks) to  the `/link/clicks` method. 

## googlquery.py ##

Takes a list of **`goo.gl`** shortened URLs and outputs tab-delimited click and expanded URL data. 

See description of `bitlyquery` above for usage.

## linkfinder.pl ##

As part of the pre-processing for link shortener research, I need to generate a list of short links from the source data. Usually, I can simply `grep` for them using something like: 

     grep -ino 'http://[a-z|0-9|/|\.|\?|=|-|_|%]*'
     
But sometimes it's important to preserve post timestamp information (particularly when processing Facebook or Twitter data.

This simple script takes a list of posts or tweets with date information in the first column. e.g.

>2014-04-30T21:28:33+0000	Strong summer style is 20% off and only a click away. Shop guys: http://asos.to/1o4sTWs Shop girls: http://asos.to/1nMFWM8 

...and outputs:

> 2014-04-30T21:28:33+0000	http://asos.to/1o4sTWs

> 2014-04-30T21:28:33+0000	http://asos.to/1nMFWM8
