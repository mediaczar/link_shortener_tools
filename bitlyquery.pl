#!/usr/bin/perl

# usage: query the bit.ly API for URL
# accepts: STDIN
# output: tab separated columns to screen: shortURL, user clicks, global clicks
# http://code.google.com/p/bitly-api/wiki/ApiDocumentation
# author: Mat Morrison
# date: Thursday September 8, 2011
# last updated: Wednesday August 29, 2012

# use modules
use LWP::Simple;
use XML::Simple;
use Data::Dumper;

# set up variables
$login='YOUR_USERNAME';
$apikey='YOUR_API_KEY';

# create object
$xml = new XML::Simple;

print join("\t","short_url","user_clicks","global_clicks","long_url\n")	;
# read each line, and make the API call
while (<STDIN>) {
	chomp;
	&callClicksAPI;
	&callExpansionAPI;
}

sub callExpansionAPI() {
	$url = 'http://api.bitly.com/v3/expand?shortUrl='.$_.'&login='.$login.'&apiKey='.$apikey.'&format=xml';

	# get XML file from bit.ly
	$content = get $url;
	die "Can't get $url" unless defined $content;

	# read XML file
	$data = $xml->XMLin($content);


	# access XML data and print TSV to screen
	# (you can fiddle with this as much
	# or as little as you like)
	print "$data->{data}->{entry}->{long_url}\n";
}

sub callClicksAPI() {
	$url = 'http://api.bitly.com/v3/clicks?shortUrl='.$_.'&login='.$login.'&apiKey='.$apikey.'&format=xml';

	# get XML file from bit.ly
	$content = get $url;
	die "Can't get $url" unless defined $content;

	# read XML file
	$data = $xml->XMLin($content);

	# access XML data and print TSV to screen
	# (you can fiddle with this as much
	# or as little as you like)
	print "$data->{data}->{clicks}->{short_url}\t";
	print "$data->{data}->{clicks}->{user_clicks}\t";
	print "$data->{data}->{clicks}->{global_clicks}\t";
}