#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fileinput
import requests
import json

def query_google_shortener(link):
	url = 'https://www.googleapis.com/urlshortener/v1/url?access_token=' + access_token + '&shortUrl=' + link + '&projection=FULL';
	query_data = requests.get(url)
	query_json = json.loads(query_data.text)
	
	result = list()	# initialise a clean list
	result.append(query_json['id'])	# shortURL
	result.append(query_json['analytics']['allTime']['shortUrlClicks'])	# clicks
	result.append(query_json['longUrl'])	# expandedURL

	return result

access_token = 'AIzaSyCy89D2WObD99ghSWkXKy4Q5yFlozPZEUY' # see https://developers.google.com/url-shortener/v1/getting_started#APIKey

headers = ("short_url", "link_clicks", "long_url")
print "\t".join(headers) # column headers

for link in fileinput.input():
	print '\t'.join(str(e) for e in query_google_shortener(link))	# join the list together, output as tab-delimited text
