#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fileinput
import requests
import json

def bitly_clicks(link):
	url = 'https://api-ssl.bitly.com/v3/link/clicks?access_token=' + access_token + '&link=' + link + '&format=json';
	query_data = requests.get(url)
	return json.loads(query_data.text)['data']['link_clicks']

def bitly_expand(link):
	url = 'https://api-ssl.bitly.com/v3/expand?access_token=' + access_token + '&shortURL=' + link + '&format=json';
	query_data = requests.get(url)
	return json.loads(query_data.text)['data']['expand'][0]['long_url']

access_token = 'YOUR ACCESS TOKEN' # see http://dev.bitly.com/authentication.html#basicauth

headers = ("short_url", "link_clicks", "long_url")
print "\t".join(headers) # column headers

for link in fileinput.input():
	result = list()	# initialise a clean list
	result.append(link.rstrip())		# shortURL
	result.append(bitly_clicks(link))	# clicks
	result.append(bitly_expand(link))	# expandedURL
	print '\t'.join(str(e) for e in result)	# join the list together, output as tab-delimited text